<!DOCTYPE html>
<html lang="en">
    @include('theme.auth_head')
    <body>
        @yield('content')
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>