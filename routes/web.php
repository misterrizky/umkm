<?php

use Illuminate\Support\Facades\Route;

Route::get('auth', 'AuthController@index')->name('auth');
Route::get('login', 'AuthController@do_login');
Route::get('register', 'AuthController@do_register');

Route::group(['middleware' => 'auth:web'], function () {
    Route::get('logout', 'AuthController@logout');
});
Route::group(['middleware' => 'auth:web','verified'], function () {
    Route::get('/', 'HomeController@index');
});